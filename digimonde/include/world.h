#ifndef WORLD_H_
#define WORLD_H_

#define ROWS 5
#define COLS 8
#define DEFAULT_TEMPERATURE 20.0


typedef struct cell_
{
    size_t col;
    size_t row;
    size_t index;
    double temperature;
} cell;


typedef struct world_
{
    char name[30];
    double year;    // years
    unsigned int nrows;
    unsigned int ncols;
    cell *worldmap; // a pointer to an array of cells
} world;



// function declarations ////////////////////////////////
world init_world();
void init_cell_values(world *theWorld);
void free_world(world *theWorld);
void printWorldMap(world *theWorld);
void printWorldTemperatures(world *theWorld);
void set_cell_temperature(world *theWorld, unsigned int irow, unsigned int icol, double t);

#endif // WORLD_H_
