#ifndef ITEM_H_
#define ITEM_H_


typedef struct item_
{
    char name[30];
    char category[20]; // food, drink, weapon, clothing, armor, etc.
    double weight;
    double volume;
    double worth;
    char modifies[20]; // the ability or stat it modifies
    int modifier; // the amount it modifies
    int hidden_factor; // how well hidden or obscured, from 0-10, with 0 = "not hidden"
} item;


// function declarations ////////////////////////////////
item init_crusty_bread();
item init_smooth_stone();
void print_item(item anItem);

#endif // ITEM_H_
