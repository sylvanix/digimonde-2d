#ifndef PLAYER_H_
#define PLAYER_H_

#include "item.h"
#include "list.h"

#define MIN_STAT_VAL 8
#define MAX_STAT_VAL 20


typedef struct location_
{
    unsigned int y;
    unsigned int x;
} location;

typedef struct player_
{
    char name[30];
    double age;    // years
    unsigned int level;
    char race[30];
    char class[30];
    unsigned int intelligence;
    unsigned int wisdom;
    unsigned int charisma;
    unsigned int strength;
    unsigned int constitution;
    unsigned int dexterity;
    unsigned int resolve; // can fluxuate with fatigue, hunger, fear, hope, proximity of allies, etc.
    unsigned int burden; // the total of the weight of the items carried and equipped
    unsigned int overburdened;
    location position;
    list_t inventory; // items carried by the player
    //equipped; // items currently equipped
} player;



// function declarations ////////////////////////////////
void initialize_player(player *aPlayer);
void free_player(player *aPlayer);
void print_inventory(list_t *list);
void set_player_position(player *aPlayer, unsigned int y, unsigned int x);
void print_player_position(location *loc);

#endif // PLAYER_H_
