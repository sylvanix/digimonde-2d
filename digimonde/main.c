/*
The world struct contains a pointer to an array of cell structs, which
we may call a "map". This array is 1-dim but has the length of ROWS*COLS,
that is, all elements of a 2-dim array. Each cell in the array contains
the 2-dim column and row indexes, as well as the 1-dim index.
*/

#include<stdio.h>  // printf
#include<string.h> // strcmp
#include "world.h"
#include "player.h"
#include "interact.h"


int main()
{
    // initialize the world
    world ourWorld = init_world();
    init_cell_values(&ourWorld);

    set_cell_temperature(&ourWorld, 4,7,31.35);

    printWorldMap(&ourWorld);
    printWorldTemperatures(&ourWorld);

    // initialize the player
    player thePlayer;
    initialize_player(&thePlayer);

    print_player_position(&thePlayer.position);
    set_player_position(&thePlayer, 3, 5);
    print_player_position(&thePlayer.position);

    item anItem;

    anItem = init_crusty_bread();
    list_append(&thePlayer.inventory, &anItem);

    anItem = init_smooth_stone();
    list_append(&thePlayer.inventory, &anItem);

    ///* The Game Loop */
    //int BUFFSZ = 64;
    //char input[BUFFSZ];
    //int CONTINUE = 1;
    //while (CONTINUE) {
    //    printf(">: ");
    //    fgets(input, BUFFSZ, stdin);
    //    if(strcmp(input, "quit\n") ==0)
    //        CONTINUE = 0;
    //    else if(strcmp(input, "walk\n") == 0)
    //        printf("    you're walking ...\n");
    //    else if(strcmp(input, "inventory\n") == 0)
    //        print_inventory(&thePlayer.inventory);
    //    else if(strcmp(input, "commands\n") == 0)
    //        print_commands();
    //    else
    //        printf("    input not recognized. Type \"commands\" for " \
    //               "a list of commands.\n");

    //}


    // don't forget to implement function to free dynamic memory!
    free_world(&ourWorld);
    free_player(&thePlayer);

    return 0;
}


