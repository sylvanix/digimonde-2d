#include <stdio.h>
#include <stdlib.h> // free()
#include <string.h>
#include "player.h"
#include "list.h"
#include "item.h"


// function definitions ////////////////////////////////

void initialize_player(player *thePlayer) {
    strcpy(thePlayer->name, "Starchild");
    thePlayer->level = 0;
    strcpy(thePlayer->race, "human");
    strcpy(thePlayer->class, "fighter");
    thePlayer->intelligence = 10; 
    thePlayer->wisdom = 10;
    thePlayer->charisma = 10;
    thePlayer->constitution = 10;
    thePlayer->dexterity = 10;
    thePlayer->resolve = 50;
    thePlayer->burden = 0;
    thePlayer->overburdened = 0;
    location loc = {0, 0};
    thePlayer->position = loc;
    list_t l;
    list_new(&l, sizeof(item), NULL);
    thePlayer->inventory = l;
}

void free_player(player *thePlayer) {
    list_free(&thePlayer->inventory);
}

void print_inventory(list_t *list) {
    size_t sz = list_size(list);
    if(sz==0) {
        puts("inventory is empty.");
    }
    else {
        printf("inventory: ");
        item anItem;
        for(unsigned long int i=0; i<sz; i++) {
            list_get_at(list, i, &anItem);
            print_item(anItem);
            if(i != sz-1) {
                printf(", ");
            }
            else printf("\n");
        }
    }
    printf("\n");
}

void set_player_position(player *thePlayer, unsigned int y, unsigned int x) {
    thePlayer->position.y = y;
    thePlayer->position.x = x;
}

void print_player_position(location *l) {
    printf("y: %d,  x: %d\n", l->y, l->x);
}
