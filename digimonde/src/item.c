#include <stdio.h>
#include <string.h>
#include "item.h"

/* instantiate "crusty bread" */
item init_crusty_bread() {
    item theItem;
    strcpy(theItem.name, "crusty bread");
    strcpy(theItem.category, "food");
    theItem.weight = 1;
    theItem.volume = 1;
    theItem.worth = 1;
    strcpy(theItem.modifies, "");
    theItem.modifier = 0;
    theItem.hidden_factor = 0;
    return theItem;
}

item init_smooth_stone(){
    item theItem;
    strcpy(theItem.name, "smooth stone");
    strcpy(theItem.category, "weapon");
    theItem.weight = 1;
    theItem.volume = 1;
    theItem.worth = 1;
    strcpy(theItem.modifies, "");
    theItem.modifier = 0;
    theItem.hidden_factor = 0;
    return theItem; 
}

void print_item(item theItem) {
    printf("%s", theItem.name);
}
