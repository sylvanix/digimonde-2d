#include <stdio.h>
#include <stdlib.h>
#include "world.h"

// function definitions ////////////////////////////////

world init_world() {
    world aWorld = {"Aarda", 1396, ROWS, COLS, malloc(sizeof(world)* ROWS * COLS)};
    return aWorld;
}


void free_world(world *theWorld) {
    free(theWorld->worldmap);
}


void init_cell_values(world *theWorld) {
    unsigned int count = 0;
    for (unsigned int i=0; i<ROWS; i++) {
        for (unsigned int j=0; j<COLS; j++) {
            theWorld->worldmap[count].row = i;
            theWorld->worldmap[count].col = j;
            theWorld->worldmap[count].index = count;
            theWorld->worldmap[count].temperature = DEFAULT_TEMPERATURE;
            count += 1;
        }
    }
}


void printWorldMap(world *theWorld) {
    printf("%s\n", theWorld->name);
    // print the world map values
    for (unsigned int i=0; i<ROWS*COLS; i++) {
        printf(
            "(%lu %lu):%lu ",
            theWorld->worldmap[i].row,
            theWorld->worldmap[i].col,
            theWorld->worldmap[i].index
        );
        if(theWorld->worldmap[i].col == COLS-1) {
            printf("\n");
        }
    }
    printf("\n"); 
}


void printWorldTemperatures(world *theWorld) {
    printf("Temperatures\n");
    // print the world map values
    for (unsigned int i=0; i<ROWS*COLS; i++) {
        printf("%f ", theWorld->worldmap[i].temperature);
        if(theWorld->worldmap[i].col == COLS-1) {
            printf("\n");
        }
    }
    printf("\n"); 
}


// TO DO: HANDLE CASE OF CELL INDEX OUT OF RANGE
void set_cell_temperature(world *theWorld, unsigned int irow, unsigned int icol, double t) {
    unsigned int index = (irow * theWorld->ncols) + icol;
    if(index >= theWorld->nrows * theWorld->ncols)
        printf("ERROR: maximum world map array size exceeded!\n");
    else
        theWorld->worldmap[index].temperature = t;
}
